﻿using System;
using ChristmasLib;
using ChristmasLib.UI;
using ChristmasLib.Utils;
using MelonLoader;

[assembly: MelonAdditionalDependencies("ChristmasLib")]
[assembly: MelonInfo(typeof(ExampleMod.ChristmasExample), "Christmas Example Mod", "1.0.0", "Christmas Gang", "https://www.Christmas.com")]
[assembly: MelonGame("VRChat", "VRChat")]
[assembly: MelonColor(ConsoleColor.Green)]


namespace ExampleMod
{
    public class ChristmasExample : ChristmasMod
    {
        public override void OnApplicationStart()
        {
            //Adds our createMenu Method to the queue of actions for menu creation
            ChristmasUI.OnUiInitActions.Add(CreateMenu);
        }
        
        private void CreateMenu()
        {
            //Creates a page with the key "Example", the page will be called Example unless changed
            //Keep in mind there cannot be duplicate keys but a page can be renamed to be the same as another
            var examplePage = ChristmasUI.AddPageByName("Example", "This is an example page");
            //Adds a single button to our example page
            examplePage.AddButton(ButtonType.SingleButton, "Single Example", "Single", () => ConsoleUtils.Write("Example button pressed!"));
            //Adds a toggle button.
            examplePage.AddButton(ButtonType.ToggleButton, "Toggle example", "Toggles", null, state => ConsoleUtils.Write(state));
        }
        
    }
    
}